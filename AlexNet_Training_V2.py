#!/usr/bin/env python
# coding: utf-8

# In[ ]:


#Necessary Imports
import os
import numpy as np
import torch
from torch.autograd import Variable
from torchvision.models.vgg import model_urls

#Importing our model from models.py
from models.alexnet import Custom_VGG

#Used to create folds out of 4 partitions of data
from itertools import combinations

# All neural network modules, nn.Linear, nn.Conv2d, BatchNorm, Loss functions
import torch.nn as nn 
import torch.nn.functional as F

# For all Optimization algorithms, SGD, Adam, etc.
import torch.optim as optim

# Loading and Performing transformations on dataset
import torchvision
import torchvision.transforms as transforms 
from torchvision.transforms import ToTensor
from torch.utils.data import Dataset, DataLoader, ConcatDataset

#Using sampler to oversample flaring instances
from torch.utils.data.sampler import Sampler, WeightedRandomSampler

#Labels in CSV and images in a folder
import pandas as pd
from PIL import Image
from datetime import datetime
import matplotlib.pyplot as plt

#For Confusion Matrix
from sklearn.metrics import confusion_matrix

#Warnings
import warnings
warnings.simplefilter("ignore", Warning)


# In[ ]:


#Dataset Loader Class
class MyJP2Dataset(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        self.annotations = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __getitem__(self, index):
        img_path = os.path.join(self.root_dir, self.annotations.iloc[index, 0])
        hmi = Image.open(img_path)

        if self.transform:
            image = self.transform(hmi)
            
        y_label = torch.tensor(int(self.annotations.iloc[index, 1]))
        
        return (image, y_label)

    def __len__(self):
        return len(self.annotations)


# In[ ]:


# CUDA initialization for PyTorch
use_cuda = torch.cuda.is_available()
device = torch.device('cuda:1' if use_cuda else 'cpu')
torch.backends.cudnn.benchmark = True
#print(device)


# In[ ]:


# Necessary Transformation
transformations = transforms.Compose([
    transforms.Resize(512),
    transforms.ToTensor()
])

#Time-Segmented tri-monthly partitioning of data into 4 partitions from Dec, 2010 - Dec, 2018

#Data from Jan-Mar
partition1 = MyJP2Dataset(csv_file = '/data/hmi_BI_daily/hmipng/labelpng/partition1.csv', 
                             root_dir = '/data/hmi_BI_daily/hmipng/',
                             transform = transformations)

#Data from Apr-Jun
partition2 = MyJP2Dataset(csv_file = '/data/hmi_BI_daily/hmipng/labelpng/partition2.csv', 
                             root_dir = '/data/hmi_BI_daily/hmipng/',
                             transform = transformations)

#Data from Jul-Sep
partition3 = MyJP2Dataset(csv_file = '/data/hmi_BI_daily/hmipng/labelpng/partition3.csv', 
                             root_dir = '/data/hmi_BI_daily/hmipng/',
                             transform = transformations)

#Data from Oct-Dec
partition4 = MyJP2Dataset(csv_file = '/data/hmi_BI_daily/hmipng/labelpng/partition4.csv', 
                             root_dir = '/data/hmi_BI_daily/hmipng/',
                             transform = transformations)


# In[ ]:


#Function to concatenate 3 out of 4 partitions into a training set
def concatdataset(data):
    concat_data = ConcatDataset(data)
    return concat_data


# In[ ]:


#Compute class weights required for oversampling
def imbalance_weights(dataset):
    target = []
    with torch.no_grad():
        for i in range(len(dataset)):
            target.append(np.asarray(dataset[i][1]))
            if i%500 == 0:
                print(f'{(i*100)/len(dataset):.2f}% Complete')
        targets = np.asarray(target)
        csc = np.array([len(np.where(targets == t)[0]) for t in np.unique(targets)])
        print(csc)
        majority_weight = 1/csc[0]
        minority_weight = 1/csc[1]
        sample_weights = np.array([majority_weight, minority_weight])
        samples_weight = sample_weights[targets]
        samples_weight = torch.from_numpy(samples_weight)
        return samples_weight

#train_samples_weight = imbalance_weights(train_set)


# In[ ]:


#Visualizing our dataset
def visualize(data_loader):
    cmap = plt.get_cmap('hmimag')
    dataiter = iter(data_loader)
    #dataiter.next()
    images, labels = dataiter.next()
    flare_types = {0: 'Non_flare', 1: 'Flare'}
    fig, axis = plt.subplots(5, 5, figsize=(15, 15))
    for i, ax in enumerate(axis.flat):
        with torch.no_grad():
            image, label = images[i], labels[i]
            ax.imshow(image.permute(1,2,0), cmap=cmap, vmin=0, vmax=1) # add image
            ax.set(title = f"{flare_types[label.item()]}")

#visualize(train_loader)


# In[ ]:


#Defining TSS and HSS as metric
def accuracy_score(prediction, target):
    TN, FP, FN, TP = confusion_matrix(target, prediction).ravel()
    print("TP: ", TP, "FP: ", FP, "TN: ", TN, "FN: ", FN)
    
    #TSS Computation also known as "recall"
    tp_rate = TP / float(TP + FN) if TP > 0 else 0  
    fp_rate = FP / float(FP + TN) if FP > 0 else 0
    TSS = tp_rate - fp_rate
    
    #HSS2 Computation
    N = TN + FP
    P = TP + FN
    HSS = (2 * (TP * TN - FN * FP)) / float((P * (FN + TN) + (TP + FP) * N))

    return TSS, HSS


# In[ ]:


#Collecting batch results into one list to compute confusion matrix, TSS, and HSS
def sklearn_Compatible_preds_and_targets(model_prediction_list, model_target_list):
    y_pred_list = []
    preds = []
    target_list = []
    tgts = []
    y_pred_list = [a.squeeze().tolist() for a in model_prediction_list]
    preds = [item for sublist in y_pred_list for item in sublist]
    target_list = [a.squeeze().tolist() for a in model_target_list]
    tgts = [item for sublist in target_list for item in sublist]
    return accuracy_score(preds, tgts)


# In[ ]:


# Training Network
def train_model(model, train_loader, val_loader, scheduler, optimizer):
    # Training Network
    print("Training in Progress..")
    train_loss_values = []
    val_loss_values = []
    train_tss_values = []
    val_tss_values = []
    train_hss_values = []
    val_hss_values = []
    for epoch in range(num_epochs):

        # setting the model to train mode
        model.train()
        train_loss = 0.
        train_tss = 0.
        train_hss = 0.
        train_prediction_list = []
        train_target_list = []
        for batch_idx, (data, targets) in enumerate(train_loader):
            # Get data to cuda if possible
            data = data.to(device=device)
            targets = targets.to(device=device)
            train_target_list.append(targets)

            # forward prop
            scores = model(data)
            loss = criterion(scores, targets)
            _, predictions = torch.max(scores,1)
            train_prediction_list.append(predictions)

            # backward prop
            optimizer.zero_grad()
            loss.backward()

            # SGD step
            optimizer.step()

            # accumulate the training loss
            #print(loss.item())
            train_loss += loss.item()


        # Validation: setting the model to eval mode
        model.eval()
        val_loss = 0.
        val_tss = 0.
        val_hss = 0.
        val_prediction_list = []
        val_target_list = []
        # Turning off gradients for validation
        with torch.no_grad():
            for d, t in val_loader:
                # Get data to cuda if possible
                d = d.to(device=device)
                t = t.to(device=device)
                val_target_list.append(t)

                # forward pass
                s = model(d)
                #print("scores", s)

                # validation batch loss and accuracy
                l = criterion(s, t)
                _, p = torch.max(s,1)
                #print("------------------------------------------------")
                #print(torch.max(s,1))
                #print('final', p)
                val_prediction_list.append(p)

                # accumulating the val_loss and accuracy
                val_loss += l.item()
                #val_acc += acc.item()
                del d,t,s,l,p
                
        #Updates Learning Rate based on stagnant val_loss
        scheduler.step(val_loss)

        #Epoch Results
        train_loss /= len(train_loader)
        train_loss_values.append(train_loss)
        val_loss /= len(val_loader)
        val_loss_values.append(val_loss)
        train_tss, train_hss = sklearn_Compatible_preds_and_targets(train_prediction_list, train_target_list)
        train_tss_values.append(train_tss)
        train_hss_values.append(train_hss)
        val_tss, val_hss = sklearn_Compatible_preds_and_targets(val_prediction_list, val_target_list)
        val_tss_values.append(val_tss)
        val_hss_values.append(val_hss)
        print(f'Epoch: {epoch+1}/{num_epochs}')
        print(f'Training--> loss: {train_loss:.4f}, TSS: {train_tss:.4f}, HSS2: {train_hss:.4f} | Val--> loss: {val_loss:.4f}, TSS: {val_tss:.4f} | HSS2: {val_hss:.4f} ')
    #plot_results(train_tss_values, val_tss_values, 'TSS') 
    #plot_results(train_loss_values, val_loss_values, 'LOSS')
    #plot_results(train_hss_values, val_hss_values, 'HSS2')
    return model, scheduler, optimizer 


# In[ ]:


def plot_results(train_values, val_values, plt_type):
    plt.plot(train_values, label='train'+str(plt_type))
    plt.plot(val_values, label='val'+str(plt_type))
    plt.xlabel('Epochs')
    plt.ylabel(plt_type)
    plt.legend()
    plt.show()


# In[ ]:


def save_model(model, scheduler, optimizer,  EPOCH, PATH):
    torch.save({
            'epoch': EPOCH,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'scheduler_state_dict': scheduler.state_dict()
            }, PATH)


# In[ ]:


def load_model():
    model = Custom_VGG()
    optimizer = optim.SGD(model.parameters(), lr=learning_rate)

    checkpoint = torch.load('load/from/path/model.pth')
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    EPOCH = checkpoint['epoch']
    return


# In[ ]:


#To carry out 4-Fold CV training, we use this function to compute the different combinations possible
def setdiff_sorted(array1,array2,assume_unique=False):
    ans = np.setdiff1d(array1,array2,assume_unique).tolist()
    if assume_unique:
        return sorted(ans)
    return ans


# In[ ]:


#Using combinations itertools to create 4C3 list of different combinations
comb = combinations(['partition1', 'partition2', 'partition3','partition4'], 3)
#print(list(comb))

#Trimonthly partitions in dictionary
dic = {'partition1': partition1,
       'partition2': partition2,
       'partition3': partition3,
       'partition4': partition4}


# In[ ]:


j=1
for i in list(comb):
    main_list = setdiff_sorted(list(dic.keys()), list(i))
    print(i, main_list)
    train, rest = [dic[x] for x in i], [dic[x] for x in main_list]
    
    #Concatenating 3-partitions
    train_set = concatdataset(train)
    val_set = rest[0]
    batch_size = 24
    train_samples_weight = imbalance_weights(train_set)
    train_sampler   = WeightedRandomSampler(weights=train_samples_weight, num_samples= len(train_samples_weight), replacement=True)
    train_loader = DataLoader(dataset=train_set, batch_size=batch_size, sampler=train_sampler,  num_workers=4, shuffle = False)
    val_loader = DataLoader(dataset=val_set, batch_size=batch_size, num_workers=4, shuffle=False)
    # Hyperparameters
    in_channel = 1

    #num_classes = 2
    learning_rate = 0.01

    #batch_size = 50
    num_epochs = 80

    #Model Initilization (Custom_VGG is just a name for model, we are using AlexNet as the architecture)
    model = Custom_VGG(ipt_size=(512, 512), pretrained=True).to(device)

    # Loss and optimizer
    criterion = nn.NLLLoss()
    optimizer = optim.SGD(model.parameters(), lr=learning_rate)
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=4)
    model, scheduler, optimizer = train_model(model, train_loader, val_loader, scheduler, optimizer)
    PATH = f"trained-models/Fold{i}.pth"
    save_model(model, scheduler, optimizer, num_epochs, PATH)
    del main_list, train, rest, train_set, val_set, train_samples_weight, train_sampler, train_loader, val_loader
    del model, optimizer, scheduler
    torch.cuda.empty_cache()
    j=j+1


# In[ ]:




