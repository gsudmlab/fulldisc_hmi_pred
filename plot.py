#!/usr/bin/env python
# coding: utf-8

# In[1]:

# All necessary imports
import os
import numpy as np
import torch
import torch.nn as nn
import sys
import torchvision
import torchvision.transforms as transforms 
import torch.nn.functional as F

import sunpy.map
from sunpy.net import Fido
from sunpy.net import attrs as a
from PIL import Image
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from astropy.visualization import astropy_mpl_style
plt.style.use(astropy_mpl_style)


# In[2]:


def original_hmi():
    """
    ===========================================
    Downloading and plotting an HMI magnetogram
    ===========================================
    """

    ###############################################################################
    # To download the required data, we use `sunpy.net.Fido`, a downloader client,
    # to query the Virtual Solar Observatory to acquire HMI data.

    result = Fido.search(a.Time('2014/12/24 12:47:05', '2014/12/24 12:48:05'),
                         a.Instrument.hmi, a.Physobs.los_magnetic_field)

    ###############################################################################
    # The following shows how to download the results. If we
    # don't provide a path it will download the file into the sunpy data directory.
    # The output provides the path of the downloaded files.

    downloaded_file = Fido.fetch(result)
    print(downloaded_file)

    ###############################################################################
    # Now load it into a map and plot it.
    # We see that solar North is pointed down instead of up in this image, which is
    # indicated by the coordinates (that range from positive to negative, rather
    # than negative to positive).

    hmi_map = sunpy.map.Map(downloaded_file[0])
    img = torch.Tensor(hmi_map.data.astype(np.float32))
    arr = np.asarray(img)
    f = plt.figure()
    f.set_figwidth(7)
    f.set_figheight(7)
    cmap = plt.get_cmap('hmimag')
    ax = plt.gca()
    im = ax.imshow(arr, cmap=cmap)
    plt.xticks([1000,2000,3000,4000])
    plt.yticks(np.arange(4000, 0, step=-1000))
    plt.xlabel('Helioprojective Longitude (Solar-X) [arcsec]')
    plt.ylabel('Helioprojective Latitude (Solar-Y) [arcsec]')
    plt.title('HMI magnetogram 2014-12-24 12:47:39')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    plt.colorbar(im, cax=cax)
    plt.show()


# In[4]:


def compressed_hmi():
    """
    ===========================================
    Plotting a compressed HMI magnetogram
    ===========================================
    """
    get_ipython().run_line_magic('pylab', 'inline')
    im1 = Image.open(r"/data/hmi_BI_daily/2014_12_24__12_48_24_305__SDO_HMI_HMI_magnetogram.png")
    arr = np.asarray(im1)
    f = plt.figure()
    f.set_figwidth(7)
    f.set_figheight(7)
    ax = plt.gca()
    im = ax.imshow(arr, cmap='gray', vmin=0, vmax=255)
    plt.xticks([1000,2000,3000,4000])
    plt.yticks(np.arange(4000, 0, step=-1000))
    plt.xlabel('Helioprojective Longitude (Solar-X) [arcsec]')
    plt.ylabel('Helioprojective Latitude (Solar-Y) [arcsec]')
    plt.title('Compressed HMI magnetogram 2014-12-24 12:47:39')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    plt.colorbar(im, cax=cax)
    plt.show()


# In[5]:

#plot original magnetogram
original_hmi()


# In[6]:

#plot compressed magnetogram
compressed_hmi()
