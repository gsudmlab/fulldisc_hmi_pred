#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Necessary Imports
import os
import numpy as np
import torch
from torch.autograd import Variable
from torchvision.models.vgg import model_urls

#Importing our model from models.py
from models.alexnet import Custom_VGG

#Used to create folds out of 4 partitions of data
from itertools import combinations

# All neural network modules, nn.Linear, nn.Conv2d, BatchNorm, Loss functions
import torch.nn as nn 
import torch.nn.functional as F

# For all Optimization algorithms, SGD, Adam, etc.
import torch.optim as optim

# Loading and Performing transformations on dataset
import torchvision
import torchvision.transforms as transforms 
from torchvision.transforms import ToTensor
from torch.utils.data import Dataset, DataLoader, ConcatDataset

#Using sampler to oversample flaring instances
from torch.utils.data.sampler import Sampler, WeightedRandomSampler

#Labels in CSV and images in a folder
import pandas as pd
from PIL import Image
from datetime import datetime
import matplotlib.pyplot as plt

#For Confusion Matrix
from sklearn.metrics import confusion_matrix

#Warnings
import warnings
warnings.simplefilter("ignore", Warning)

import pathlib


# In[2]:

#Pytorch based custom Dataset Loader
class MyJP2Dataset(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        self.annotations = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __getitem__(self, index):
        img_path = os.path.join(self.root_dir, self.annotations.iloc[index, 0])
        hmi = Image.open(img_path)

        if self.transform:
            image = self.transform(hmi)
            
        y_label = torch.tensor(int(self.annotations.iloc[index, 1]))
        
        return image, y_label, img_path

    def __len__(self):
        return len(self.annotations)


# In[3]:


# Transformations applied to images
transformations = transforms.Compose([
    transforms.Resize(512),
    transforms.ToTensor()
])

#loading images for inference
test_set = MyJP2Dataset(csv_file = 'path/to/labels.csv', 
                             root_dir = 'path/to/images/',
                             transform = transformations)
test_loader = DataLoader(dataset=test_set, batch_size=24, num_workers=4, shuffle=True)

# In[8]:

#Cuda initialization
use_cuda = torch.cuda.is_available()
device = torch.device('cuda:1' if use_cuda else 'cpu')
torch.backends.cudnn.benchmark = True

#Loading trained models for inference
model = Custom_VGG().to(device)
PATH = 'trained-models/lessthanM-best.pth'

optimizer = optim.SGD(model.parameters(), lr=0.01)
checkpoint = torch.load(PATH)
model.load_state_dict(checkpoint['model_state_dict'])
optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
epoch = checkpoint['epoch']

#For inference model should be in evaluation mode
model.eval()


# In[13]:

#We use this function to make predictions from trained model and 
#plot the images with their actual flare intensity and predicted output
def visualize_model(model):
    cmap = plt.get_cmap('binary')
    data = pd.read_csv (r'path/to/labels/containing/actual/flare/intensity.csv')   
    df = pd.DataFrame(data, columns= ['label','goes_class'])
    x_countT, x_countF, m_countT, m_countF, c_countT, c_countF = 0,0,0,0,0,0

    #Empty space and nan values are filled with 0 in the goes_class column
    df.fillna(str(None), inplace=True)
    df.replace(r'\s+', np.nan, regex=True)
    flare_types = {0: 'Non_flare', 1: 'Flare'}
    model.eval()
    with torch.no_grad():
        for i, (inputs, labels, path) in enumerate(test_loader):
            inputs = inputs.to(device)
            labels = labels.to(device)
            l = labels.tolist()
            path_str = list(path)
            label_name=pathlib.PurePath(path_str[0]).name[5:24]
            date_time_obj = datetime.strptime(label_name, '%Y.%m.%d_%H.%M.%S').strftime('%Y-%m-%d %H:%M:%S')
            #print(date_time_obj)

            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)
            preds = preds.tolist()
            #print('labels:', l )
            #print('preds:', preds)
            #print(path)

            fig, axis = plt.subplots(6, 4, figsize=(20, 20))
            for i, ax in enumerate(axis.flat):
                image, label = inputs[i].cpu(), l[i]
                label_name=pathlib.PurePath(path_str[i]).name[5:24]
                date_time_obj = datetime.strptime(label_name, '%Y.%m.%d_%H.%M.%S').strftime('%Y-%m-%d %H:%M:%S')
                date_time_obj = str(date_time_obj)
                actual, predicted = flare_types[label], flare_types[preds[i]]
                flare = df.loc[df['label'] == date_time_obj].goes_class.to_string(index=False)
                if 'X' in flare:
                    if actual==predicted:
                        x_countT+=1
                    else:
                        x_countF+=1
                if 'M' in flare:
                    if actual==predicted:
                        m_countT+=1
                    else:
                        m_countF+=1
                if 'C' in flare:
                    if actual==predicted:
                        c_countT+=1
                    else:
                        c_countF+=1
                ax.imshow(image.permute(1,2,0), cmap=cmap, vmin=0, vmax=1) # add image
                ax.set(title = f"Actual: {actual}({flare})| Predicted: {predicted}")
    print("Correct X: ", x_countT)
    print("InCorrect X: ", x_countF)
    print("Correct M: ", m_countT)
    print("InCorrect M: ", m_countF)
    print("Correct C: ", c_countT)
    print("InCorrect C: ", c_countF)


# In[ ]:


visualize_model(model)


